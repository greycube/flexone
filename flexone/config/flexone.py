from frappe import _

def get_data():
	return [
			
		{
			"label": _("Advance Tools"),
			"items": [
				{
					"type": "page",
					"name": "dashboard",
					"label": _("Dashboard"),
				},
				{
					"type": "doctype",
					"name": "Administrative Decision",
					"label": _("Administrative Decision"),
					"description": _("Administrative Decision"),
					"hide_count": False
				},
				{
					"type": "doctype",
					"name": "Admin Document Type",
					"label": _("Admin Document Type"),
					"description": _("Admin Document Type"),
					"hide_count": False
				}
			]
		},
			{
			"label": _("Quick Accounts"),
			"icon": "fa fa-print",
			"items": [
				{
					"type": "doctype",
					"name": "Expense Entry",
					"label": _("Expense Entry"),
					"description": _("Expense Entry"),
					"hide_count": False
				}
			]
		}
    ]
